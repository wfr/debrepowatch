# debrepowatch
Monitor and log changes in a Debian repository.

## Preparation
```
    aptitude install python3 curl
```

## Usage
The `debrepowatch` script monitors the Packages.xz file of Debian repositories and logs updates in an sqlite database. Changes are printed on stdout.

The default configuration monitors debian-unstable and debian-testing. To configure custom repositories, copy `repositories.ini.example` to `repositories.ini` and edit it.

Simply run `./debrepowatch`.
